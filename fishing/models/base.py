from abc import ABCMeta, abstractmethod

class BaseModel(metaclass=ABCMeta):
    @abstractmethod
    def fit(self, data):
        pass
