from fishing.models.base import BaseModel
from fishing.params import CParams
from sklearn.model_selection import train_test_split
import numpy as np
from keras.models import Model
from keras.layers import Input, Flatten, Dense, concatenate, \
    Dropout, BatchNormalization
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard, Callback
import fishing.const as const
from fishing.y2bin import Y2bin
from fishing.scoring import scoring
from fishing.models.roc_callback import roc_callback
from fishing.features.encoding import equalwidth_encoding
import datetime, time
from pathlib import Path

EMBEDDING_DIM = 300
DROPOUT = 0.1
DENSE_SIZE = 200

TEST_SPLIT = 0.2
RNG_SEED = 42
VOCAB_SIZE = 2000
batch_size = 64
LSTM_UNITS = 256
NUM_BINS = 50

class LSTMModel(BaseModel):
    """
    split_train_test
    create graph
    run fit
    """
    def __init__(self, cparams:CParams, model_weights_dir, tensorboard_dir):
        self.cparams = cparams
        self.model_weights_dir = model_weights_dir
        self.tensorboard_dir = tensorboard_dir
        self.lb = Y2bin(const.ALL_CLASSES)
        #self.features_cols = [1,2,3,4,5,6]
        self.build_model()

    def get_model_file(self, ext=".h5"):
        return str(self.model_weights_dir / Path(str(self.cparams)+ext))

    #TODO refactor it
    def fit(self, data):
        self.split_train_test(data)
        #TODO try using different optimizers and different optimizer configs
        #'categorical_crossentropy'
        self.model.compile(loss='categorical_crossentropy',
                      optimizer='rmsprop',
                      metrics=['accuracy'])
        print(len(self.X_train[0]), 'train sequences')
        print(len(self.X_test[0]), 'test sequences')
        print(self.X_train[0])

        early_stopping = EarlyStopping(monitor='val_loss', patience=4)
        model_checkpoint = ModelCheckpoint(self.get_model_file(), monitor='val_acc', save_best_only=True)
        tensor_board = TensorBoard(log_dir=str(self.tensorboard_dir / Path(str(self.cparams))),
                                   histogram_freq=0, write_graph=True, write_images=True)
        roc_ = roc_callback(self.X_train, self.y_train, self.X_test, self.y_test)
        callbacks = [early_stopping,  model_checkpoint, tensor_board, roc_]
        print("Starting training at", datetime.datetime.now())
        t0 = time.time()
        self.model.fit(self.X_train, self.y_train,
                  batch_size=batch_size,
                  epochs=50,
                  validation_data=(self.X_test, self.y_test),
                  callbacks=callbacks)
        t1 = time.time()
        print("Training ended at", datetime.datetime.now())
        print("Minutes elapsed: %f" % ((t1 - t0) / 60.))

        self.model.load_weights(self.get_model_file())

        score, acc = self.model.evaluate(self.X_test, self.y_test,
                                    batch_size=batch_size)
        print('Test score:', score)
        print('Test accuracy:', acc)

    def predict(self, test_data):
        ids, arr_X = test_data
        print("Make predictions...")
        self.model.load_weights(self.get_model_file())
        _data = arr_X[3:6]
        _data[0] = equalwidth_encoding(_data[0], NUM_BINS)
        y_pred = self.model.predict(arr_X[3:6], batch_size=batch_size)
        return ids, y_pred

    def calc_score(self, data):
        self.split_train_test(data)
        self.model.load_weights(self.get_model_file())
        y_pred = self.model.predict(self.X_test, batch_size=batch_size)
        return scoring(self.y_test, y_pred)


    def split_train_test(self, data):
        ids, arr_X, y = data
        X = np.hstack(arr_X).astype('float32')
        _X_train, _X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SPLIT, random_state=RNG_SEED)
        self.X_train = np.hsplit(_X_train, len(arr_X))[3:6]

        self.X_train[0] = equalwidth_encoding(self.X_train[0], NUM_BINS)
        self.X_test = np.hsplit(_X_test, len(arr_X))[3:6]
        self.X_test[0] = equalwidth_encoding(self.X_test[0], NUM_BINS)
        self.y_train, self.y_test = self.lb.transform(y_train), self.lb.transform(y_test)

    def build_model(self):
        self.model = self.model(self.cparams.seqlen, len(const.ALL_CLASSES))
        self.model.summary()
        print("Inputs: {}".format(self.model.input_shape))

    def model(self, seqlen, num_classes):
        """
        "Latitude", - 1
        "Longitude", - 2
        "SOG", - 3
        oceanic depth - 4,
        Chlorophyll Concentration - 5
        """
        def EncodeInput(_inp, name):
            th = Embedding(VOCAB_SIZE,
                         EMBEDDING_DIM,
                         input_length=seqlen, #input_dim=NUM_BINS,
                         trainable=True)(_inp)
            th = LSTM(LSTM_UNITS, input_dim=4, dropout=0.2, return_sequences=True, recurrent_dropout=0.2)(th)
            th = Flatten()(th)
            return th



        # Define the model
        #lat_idx__inp = Input(shape=(seqlen,), name="lat")
        #lon_idx__inp = Input(shape=(seqlen,), name="lon")
        sog_idx__inp = Input(shape=(seqlen,NUM_BINS), name="sog")
        od_idx__inp = Input(shape=(seqlen,), name="od")
        cc_idx__inp = Input(shape=(seqlen,), name="cc")

        #lat = EncodeInput(lat_idx__inp, "lat")
        #lon = EncodeInput(lon_idx__inp, "lon")
        sog = EncodeInput(sog_idx__inp, "SOG")
        od = EncodeInput(od_idx__inp, "OD")
        cc = EncodeInput(cc_idx__inp, "CC")



        #merged = concatenate([lat,lon,sog,od,cc])
        merged = concatenate([sog,od,cc])
        merged = Dense(DENSE_SIZE, activation='relu')(merged)
        merged = Dropout(DROPOUT)(merged)
        merged = BatchNormalization()(merged)
        merged = Dense(DENSE_SIZE, activation='relu')(merged)
        merged = Dropout(DROPOUT)(merged)
        merged = BatchNormalization()(merged)

        vessel_type  = Dense(output_dim=num_classes, activation='softmax', name="vessel_type")(merged)

        model = Model(inputs=[#lat_idx__inp,
                              #lon_idx__inp,
                              sog_idx__inp,
                              od_idx__inp,
                              cc_idx__inp,
                              ],  outputs=[vessel_type])
        return model


