from keras.callbacks import Callback
from fishing.scoring import scoring

class roc_callback(Callback):
    """
    callbacks=[roc_callback(training_X, training_y, validation_X, validation_y)]
    """
    def __init__(self, training_X, training_y, validation_X, validation_y):
        self.x, self.y, self.x_val, self.y_val = training_X, training_y, validation_X, validation_y

    def on_train_begin(self, logs={}):
        return

    def on_train_end(self, logs={}):
        return

    def on_epoch_begin(self, epoch, logs={}):
        return

    def on_epoch_end(self, epoch, logs={}):
        #print("on_epoch_end {}".format(epoch))
        #y_pred = self.model.predict(self.x)
        #roc = scoring(self.y, y_pred)

        y_pred_val = self.model.predict(self.x_val)
        #roc_val = roc_auc_score(self.y_val, y_pred_val)
        roc_val = scoring(self.y_val, y_pred_val)

        print('\rroc-auc: %s' % str(roc_val), end=100*' '+'\n')
        #print('\n'+'\rroc-auc: %s - roc-auc_val: %s' % (str(round(roc,4)),str(round(roc_val,4))),end=100*' '+'\n')
        return

    def on_batch_begin(self, batch, logs={}):
        return

    def on_batch_end(self, batch, logs={}):
        return


