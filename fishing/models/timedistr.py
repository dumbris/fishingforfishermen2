from fishing.models.base import BaseModel
from fishing.params import CParams
from sklearn.model_selection import train_test_split
import numpy as np
from keras.models import Model
from keras.layers import Input, TimeDistributed, Dense, Lambda, concatenate, \
    Dropout, BatchNormalization
from keras.layers.embeddings import Embedding
from keras import backend as K
import const
from y2bin import Y2bin

EMBEDDING_DIM = 35
DROPOUT = 0.07
DENSE_SIZE = 350

TEST_SPLIT=0.1
RNG_SEED=42
VOCAB_SIZE=20000
batch_size = 32

class LSTMModel(BaseModel):
    """
    split_train_test
    create graph
    run fit
    """
    def __init__(self, cparams:CParams):
        self.cparams = cparams
        self.lb = Y2bin(const.ALL_CLASSES)
        self.build_model()

    def fit(self, data):
        self.split_train_test(data)
        # try using different optimizers and different optimizer configs
        self.model.compile(loss='categorical_crossentropy',
                      optimizer='adam',
                      metrics=['accuracy'])
        print(len(self.X_train[0]), 'train sequences')
        print(len(self.X_test[0]), 'test sequences')
        print('Train...')
        self.model.fit(self.X_train, self.y_train,
                  batch_size=batch_size,
                  epochs=30,
                  validation_data=(self.X_test, self.y_test))
        score, acc = self.model.evaluate(self.X_test, self.y_test,
                                    batch_size=batch_size)
        print('Test score:', score)
        print('Test accuracy:', acc)

    def predict():
        pass

    def split_train_test(self, data):
        ids, arr_X, y = data
        X = np.hstack(arr_X)
        _X_train, _X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SPLIT, random_state=RNG_SEED)
        self.X_train = np.hsplit(_X_train, len(arr_X))[1:4] #WARN: Select only 3 columns!
        self.X_test = np.hsplit(_X_test, len(arr_X))[1:4]
        self.y_train, self.y_test = self.lb.transform(y_train), self.lb.transform(y_test)

    def build_model(self):
        self.model = self.model(self.cparams.seqlen, len(const.ALL_CLASSES))

    def model(self, seqlen, num_classes):
        """
        "Latitude", - 1
        "Longitude", - 2
        "SOG", - 3
        """
        def EncodeInput(_inp, name):
            th = Embedding(VOCAB_SIZE,
                         EMBEDDING_DIM,
                         input_length=seqlen,
                         trainable=True)(_inp)
            th = TimeDistributed(Dense(EMBEDDING_DIM, activation='relu'))(th)
            th = Lambda(lambda x: K.mean(x, axis=1), output_shape=(EMBEDDING_DIM, ), name="Mean_"+name)(th)
            return th



        # Define the model
        lat_idx__inp = Input(shape=(seqlen,), name="lat")
        lon_idx__inp = Input(shape=(seqlen,), name="lon")
        sog_idx__inp = Input(shape=(seqlen,), name="sog")

        lat = EncodeInput(lat_idx__inp, "lat")
        lon = EncodeInput(lon_idx__inp, "lon")
        sog = EncodeInput(sog_idx__inp, "SOG")



        merged = concatenate([lat,lon,sog])
        merged = Dense(DENSE_SIZE, activation='relu')(merged)
        merged = Dropout(DROPOUT)(merged)
        merged = BatchNormalization()(merged)

        vessel_type  = Dense(output_dim=num_classes, activation='softmax', name="vessel_type")(merged)

        model = Model(inputs=[lat_idx__inp, lon_idx__inp, sog_idx__inp],  outputs=[vessel_type])
        return model
