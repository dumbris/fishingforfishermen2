import bcolz
from pathlib import Path
#from typing import List
from fishing.params import CParams
from fishing.helpers import merge_data, split_data


class ArrayCache:
    """
        try:
            X = load_arr((prefix, size, limit))
        except FileNotFoundError as e:
            print(e)
            X = build_cache(img_path, (prefix, size, limit), X_train)
        self.X = X
    """
    def __init__(self, base_dir: Path):
        self.base_dir = base_dir

    def get_cache_path(self, cache_param: CParams) -> Path:
        return self.base_dir / '{}.bc'.format(str(cache_param))

    def save_arr(self, arr, cache_param: CParams, attrs={}):
        cache_dir = self.get_cache_path(cache_param)
        if not cache_dir.parent.exists():
            cache_dir.parent.mkdir()
        return self.save_array(cache_dir, arr, attrs)


    def load_arr(self, cache_param: CParams):
        cache_dir = self.get_cache_path(cache_param)
        if not cache_dir.parent.exists():
            raise FileNotFoundError("No cache %s" % cache_dir)
        return self.load_array(cache_dir)


    def save_array(self, fname, arr, attrs={}):
        mode_train = True
        if len(arr) < 3:
            mode_train = False
        a = merge_data(arr, mode_train)
        print(a.shape)
        c=bcolz.carray(a, rootdir=str(fname), mode='w')
        c.attrs["cparams"] = attrs
        c.attrs["mode_train"] = mode_train
        c.attrs["numcols"] = len(arr[1])
        print("c.flush()")
        c.flush()
        return c

    def load_array(self, fname):
        c = bcolz.open(str(fname))
        #TODO remove:w

        numcols = 6
        if "numcols" in c.attrs:
            numcols = c.attrs["numcols"]
        mode_train = True
        if "mode_train" in c.attrs:
            mode_train = c.attrs["mode_train"]
        print("Split loaded data into {} cols".format(numcols))
        return split_data(c[:], numcols, mode_train)


