
# coding: utf-8

# In[1]:

import bcolz
#import numpy as np
from torch import np # Torch wrapper for Numpy
import sys
import math
import random
import PIL
import cv2
import os
from sklearn.preprocessing import MultiLabelBinarizer
import pandas as pd

from pathlib import Path
from typing import Dict, Tuple
from PIL import Image
import matplotlib.pyplot as plt
import imgaug as ia
from imgaug import augmenters as iaa

import torchvision
import torch
from torch.utils.data import DataLoader, Dataset
import torchvision.models as models


import os
from PIL import Image

from torchvision import transforms
from torch import nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable


from bcolz_array_iterator import BcolzArrayIterator


from matplotlib.pyplot import imshow

get_ipython().magic('matplotlib inline')


Size = Tuple[int, int]

Limit = int

Prefix = str

CacheParam = Tuple[Prefix, Size, Limit]

ORIG_SIZE:Size = (256, 256)
    
SIZE:Size = (256, 256)
    
SIZE2:Size = (228, 228)    


BASE_DIR = Path("/ssd_data/kagle/planet-understanding-the-amazon-from-space/input")

IMG_EXT = '.jpg'
TRAIN_DATA = Path(BASE_DIR, 'train_v2.csv')

CACHE_DATA = Path(BASE_DIR, 'cache')


# In[2]:

#cached_path = path.parent / '{}-{}'.format(*size) / '{}.jpg'.format(path.stem)

def get_path(x, img_path: Path, img_ext = ".jpg") -> Path:
    return img_path / Path(x + img_ext)

def read_train_csv(csv_path):
    tmp_df = pd.read_csv(csv_path)
    
    mlb = MultiLabelBinarizer()
    X_train = tmp_df['image_name']
    y_train = mlb.fit_transform(tmp_df['tags'].str.split()).astype(np.float32)
    return X_train, y_train

def load_image(path: Path) -> Image.Image:
    return Image.open(str(path)).convert('RGB')

def load_image_resized(path: Path, size: Size) -> Image.Image:
    img = load_image(path)
    if ORIG_SIZE != size:
        return img.resize(size, resample=Image.BILINEAR)
    return img


def save_array(fname, arr):
    c=bcolz.carray(arr, rootdir=fname, mode='w', chunklen=32)
    c.flush()
    return c
    
def load_array(fname): return bcolz.open(fname)#[:]


def get_cache_path(cache_param: CacheParam, path: Path = CACHE_DATA) -> Path:
    prefix, size, limit = cache_param
    if limit:
        return path / '{}_{}_{}_{}.bc'.format(prefix, *size, limit)
    return path / '{}_{}_{}_nolim.bc'.format(prefix, *size)

def save_images(arr, cache_param: CacheParam):
    cache_dir = get_cache_path(cache_param)
    if not cache_dir.parent.exists():
        cache_dir.parent.mkdir()
    return save_array(cache_dir, arr)
    

def load_images(cache_param: CacheParam):
    cache_dir = get_cache_path(cache_param)
    if not cache_dir.parent.exists():
        raise FileNotFoundError("No cache %s" % cache_dir)
    return load_array(cache_dir)



# In[6]:

def tensor_to_img(img, mean=0, std=1, dtype=np.uint8):
    img = img.numpy()
    img = np.transpose(img, (1, 2, 0))
    img = img*std + mean
    img = img.astype(dtype)
    #img = cv2.cvtColor(img , cv2.COLOR_BGR2RGB)
    return img

## transform (input is numpy array, read in by cv2)
def img_to_tensor(img, mean=0, std=1.):
    img = img.astype(np.float32)
    img = (img-mean)/std
    img = img.transpose((2,0,1))
    tensor = torch.from_numpy(img)   ##.float()
    return tensor


sometimes = lambda aug: iaa.Sometimes(0.5, aug)


seq = iaa.Sequential([
    iaa.Crop(px=(0, 16)), # crop images from each side by 0 to 16px (randomly chosen)
    iaa.Fliplr(0.5), # horizontally flip 50% of the images
    iaa.GaussianBlur(sigma=(0, 3.0)), # blur images with a sigma of 0 to 3.0
    iaa.Flipud(0.5), # vertically flip 20% of all images
    sometimes(iaa.Crop(percent=(0, 0.1))), # crop images by 0-10% of their height/width
    sometimes(iaa.Affine(
            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}, # scale images to 80-120% of their size, individually per axis
            translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)}, # translate by -20 to +20 percent (per axis)
            rotate=(-45, 45), # rotate by -45 to +45 degrees
            shear=(-16, 16), # shear by -16 to +16 degrees
            order=[0, 1], # use nearest neighbour or bilinear interpolation (fast)
            cval=(0, 255), # if mode is constant, use a cval between 0 and 255
            mode=ia.ALL # use any of scikit-image's warping modes (see 2nd image from the top for examples)
    )),
    ])


def build_cache(img_path: Path, cache_param: CacheParam, X_train, img_ext: str = ".jpg"):
    size = cache_param[1]
    num = len(X_train.index)
    print("Building cache for size {} limit {}".format(*cache_param))
    assert X_train.apply(lambda x: os.path.isfile(get_path(x, img_path))).all(),             "Some images referenced in the CSV file were not found"
    
    channel = 3
    images = np.zeros((num,size[0],size[1],channel),dtype=np.uint16)
    for n in range(num):
        images[n] = load_image_resized(img_path / Path(X_train[n] + img_ext), size)

    save_images(images, cache_param)
    
    del images

class KaggleDataset(Dataset):
    def __init__(self, img_path: Path, size: Size, aug, prefix: str = 'train', limit=None,                  transformations=None, labels_path: Path = TRAIN_DATA):
        X_train, y_train = read_train_csv(labels_path)
        if limit:
            X_train = X_train[:limit]
            y_train = y_train[:limit]
        self.channel = 3 #jpg
        self.size = size
        self.num = len(X_train.index)
        self.labels = y_train
        
        self.aug = aug
        self.transformations = transformations
        try:
            X = load_images((prefix, size, limit))
        except FileNotFoundError as e:
            print(e)
            X = build_cache(img_path, (prefix, size, limit), X_train)
        self.X = X

    def __len__(self):
        return self.num

    def __getitem__(self, idx):
        img = self.aug.augment_image(self.X[idx])
        if self.transformations:
            for t in self.transformations:
                img = t(img)
            #print(type(self.transformations))
            #img = self.transformations(img)
        else:
            img = torch.from_numpy(img)
            
        return img, torch.from_numpy(self.labels[idx])


# In[7]:

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 32, kernel_size=3)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(2304, 256)
        self.fc2 = nn.Linear(256, 17)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(x.size(0), -1) # Flatten layer
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.sigmoid(x)

class Amazon(nn.Module):
    def __init__(self, pretrained_model_1):
        super(Amazon, self).__init__()
        self.pretrained_model_1 = pretrained_model_1
        # self.pretrained_model_2 = pretrained_model_2
        self.relu = nn.ReLU()
        self.fc1 = nn.Linear(2000,1000)
        self.fc2 = nn.Linear(1000,17) # create layer
        self.sigmoid = torch.nn.Sigmoid()
    def forward(self, x):
        model_1 = self.relu(self.pretrained_model_1(x))
        #model_2 = self.relu(self.pretrained_model_2(x))
        #out1 = torch.cat((model_1,model_2),1)
        return self.sigmoid(self.fc2(self.relu(model_1)))

#pretrained_model1 = models.densenet169(pretrained=True)
pretrained_model1 = models.resnet18(pretrained=True)#in fact, this should be set as true




  

def train(epoch, train_loader):
    model.train()
    #print(type(train_loader))
    criterion = nn.BCELoss().cuda()
    
    best_loss = 10
    path = BASE_DIR.parent / 'best_loss.pkl'
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.cuda(async=True), target.cuda(async=True) # On GPU
        data, target = Variable(data), Variable(target)
        optimizer.zero_grad()
        output = model(data)
        loss = F.binary_cross_entropy(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % 100 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.data[0]))
            if loss.data[0] < best_loss:
                best_loss = loss.data[0]
                torch.save(model.state_dict(), str(path))
                print("save in : "+ str(path))

            
img_path = BASE_DIR / Path("train-jpg")
#transformations = transforms.Compose([transforms.Scale(32),transforms.ToTensor()])
#transformations = transforms.Compose([transforms.ToTensor()])
kgd = KaggleDataset(img_path, size=(224,224), aug=seq, limit=5000, transformations=[
                                        lambda x: img_to_tensor(x)
                                    ], prefix='train')
            
train_loader = DataLoader(kgd,
                              batch_size=8,
                              shuffle=True,
                              num_workers=1, # 1 for CUDA
                              pin_memory=True # CUDA only
                             )

    
#model = Net().cuda() # On GPU
model = Amazon(pretrained_model1).cuda()    

#optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.5)
optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)

for epoch in range(1, 2):
    train(epoch, train_loader)


# In[ ]:

get_ipython().magic('debug')


# In[ ]:

def train(args, model: nn.Module, criterion, *, train_loader, valid_loader,
          validation, init_optimizer, save_predictions=None, n_epochs=None,
          patience=2):
    lr = args.lr
    n_epochs = n_epochs or args.n_epochs
    optimizer = init_optimizer(lr)

    root = Path(args.root)
    model_path = root / 'model.pt'
    best_model_path = root / 'best-model.pt'
    if model_path.exists():
        state = torch.load(str(model_path))
        epoch = state['epoch']
        step = state['step']
        best_valid_loss = state['best_valid_loss']
        model.load_state_dict(state['model'])
        print('Restored model, epoch {}, step {:,}'.format(epoch, step))
    else:
        epoch = 1
        step = 0
        best_valid_loss = float('inf')

    save = lambda ep: torch.save({
        'model': model.state_dict(),
        'epoch': ep,
        'step': step,
        'best_valid_loss': best_valid_loss
    }, str(model_path))

    report_each = 10
    save_prediction_each = report_each * 20
    log = root.joinpath('train.log').open('at', encoding='utf8')
    valid_losses = []
    lr_reset_epoch = epoch
    for epoch in range(epoch, n_epochs + 1):
        model.train()
        random.seed()
        tq = tqdm.tqdm(total=(args.epoch_size or
                              len(train_loader) * args.batch_size))
        tq.set_description('Epoch {}, lr {}'.format(epoch, lr))
        losses = []
        tl = train_loader
        if args.epoch_size:
            tl = islice(tl, args.epoch_size // args.batch_size)
        try:
            mean_loss = 0
            for i, (inputs, targets) in enumerate(tl):
                inputs, targets = variable(inputs), variable(targets)
                outputs = model(inputs)
                loss = criterion(outputs, targets)
                optimizer.zero_grad()
                batch_size = inputs.size(0)
                (batch_size * loss).backward()
                optimizer.step()
                step += 1
                tq.update(batch_size)
                losses.append(loss.data[0])
                mean_loss = np.mean(losses[-report_each:])
                tq.set_postfix(loss='{:.3f}'.format(mean_loss))
                if i and i % report_each == 0:
                    write_event(log, step, loss=mean_loss)
                    if save_predictions and i % save_prediction_each == 0:
                        p_i = (i // save_prediction_each) % 5
                        save_predictions(root, p_i, inputs, targets, outputs)
            write_event(log, step, loss=mean_loss)
            tq.close()
            save(epoch + 1)
            valid_metrics = validation(model, criterion, valid_loader)
            write_event(log, step, **valid_metrics)
            valid_loss = valid_metrics['valid_loss']
            valid_losses.append(valid_loss)
            if valid_loss < best_valid_loss:
                best_valid_loss = valid_loss
                shutil.copy(str(model_path), str(best_model_path))
            elif (patience and epoch - lr_reset_epoch > patience and
                  min(valid_losses[-patience:]) > best_valid_loss):
                # "patience" epochs without improvement
                lr /= 5
                lr_reset_epoch = epoch
                optimizer = init_optimizer(lr)
        except KeyboardInterrupt:
            tq.close()
            print('Ctrl+C, saving snapshot')
            save(epoch)
            print('done.')
            return

