from pathlib import Path
import numpy as np
from typing import List

SKIP_IDS = [100846]

class DataReader():
    TRACKS_DIR = Path("VesselTracks")

    def __init__(self,  base_dir: Path, cache_dir: Path, cols):
        self.base_dir = base_dir
        self.cache_dir = cache_dir
        self.cols = cols #["TrackNumber","Time(seconds)","Latitude","Longitude"] = [0:4] or [:,[0,1,2,3,4]]


    def get_data_train(self, limit=0):
        """
        Returns raw X,y as in data set
        """
        ids, y = self.read_train(Path("training.txt"), limit)
        arr = self.load_train(ids, y)
        print("Loaded {} items. Col nums {}".format(*arr.shape))
        return arr

    def get_data_test(self):
        """
        Returns raw X as in data set
        """
        ids = self.read_test(Path("testing.txt"))
        arr = self.load_test(ids)
        print("Loaded {} items for test. Col nums {}".format(*arr.shape))
        return arr

    def read_train(self, filename: Path, limit=0):
        fullpath = self.base_dir / filename
        ids = []
        y = []
        with open(str(fullpath), 'r') as f:
            i = 0
            for line in f:
                try:
                    _id, _class = line.rstrip('\n').split(',')
                except Exception as e:
                    print(e)
                if int(_id) in SKIP_IDS:
                    print('{} skiped'.format(_id))
                    continue
                ids.append(_id)
                y.append(_class)
                i += 1
                if limit and limit > 0:
                    if limit <= i:
                        print("Reached the limit {}".format(limit))
                        break
        print("{} items readed from train file {}".format(len(ids), fullpath))
        return ids, y

    def read_test(self, filename: Path):
        fullpath = self.base_dir / filename
        ids = []
        with open(str(fullpath), 'r') as f:
            for line in f:
                try:
                    _id = line.rstrip('\n')
                except Exception as e:
                    print(e)
                ids.append(_id)
        print("{} items readed from test file {}".format(len(ids), fullpath))
        return ids


    def load_train(self, ids: List[int], y: List[str]):
        res = []
        for _id, _y in zip(ids, y):
            #BEWARE [0:4]
            arr = self.read_csv(_id)[:,self.cols]
            arr_y = np.full((arr.shape[0], 1), _y)
            res.append(np.hstack([arr,arr_y]))
        return np.vstack(res)

    def load_test(self, ids: List[int]):
        res = []
        for _id in ids:
            #BEWARE [0:4]
            arr = self.read_csv(_id)[:,self.cols]
            res.append(arr)
        return np.vstack(res)

    def read_csv(self, _id):
        name = self.base_dir / self.TRACKS_DIR / Path("{}.csv".format(_id))
        #print("Read {}".format(name))
        return np.genfromtxt(str(name), delimiter=',', skip_header=1)
