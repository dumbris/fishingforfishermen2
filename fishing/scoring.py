from sklearn import metrics
import numpy as np


def scoring(y_test, y_pred):
    """
    Scoring
    Your predictions will be scored against the ground truth using the area under the receiver operating characteristic (ROC). Some of the records in the test set will be used for provisional scoring, and others for system test scoring. (You will not know which records belong to which set.)

    The ROC curve will be determined and the score will be determined from the area under the ROC curve using the following method:

    The contestant's submission will score each track with a probability that the vessel was engaged in each type of fishing.
    Each fishing type will be treated as a binary classifier, and it’s AuC will be calculated as in steps 3-5.
    The true positive rates and the false positive rates are determined as follows:
    TPR_i = Accumulate[s_i] / N_TPR
    FPR_i = Accumulate[1 - s_i] / N_FPR;
    with the addition: FPR_0 = 0;
    where N_TRP is the total number of fishing records of the given type, N_FPR is the total number of records not of that fishing type, and N_TRP + N_FPR = N (total number of records with known status in the test)
    Then the AuC is determined as a numerical integral of TRP over FRP:

    AuC = Sum [TPR_i * (FPR_i - FPR_i-1)]
    Then the four AuC values are weighted and averaged, then scaled to determine the final score:

    Score = max(1,000,000 * (2 * WeightedAverage - 1), 0)
    The weights are as follows:

    trawler: 40%
    seiner: 30%
    longliner: 20%
    support: 10%
    """
    assert y_pred.shape == y_test.shape
    weights = np.array([0.2, 0, 0.3, 0.1, 0.4])
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = np.zeros((y_pred.shape[1]))
    for i in range(0,y_pred.shape[1]):
        fpr[i], tpr[i], _ = metrics.roc_curve(y_test[:,i], y_pred[:,i])
        roc_auc[i] = metrics.auc(fpr[i], tpr[i])

    return np.max(1000000 * (2*np.average(roc_auc, weights=weights) - 1), 0)
