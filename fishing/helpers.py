import numpy as np

def split_data_train(arr):
    y = arr[:,-1:]
    ids = arr[:,:1]
    data = arr[:,1:-1]
    return ids, data, y

def split_data_test(arr):
    ids = arr[:,:1]
    data = arr[:,1:]
    return ids, data

def merge_data(arr, mode_train=True):
    print(type(arr))
    if type(arr) not in ['tuple', 'list']:
        print('Wrong format')
        #raise Exception('Wrong format')
    if mode_train:
        return np.hstack([arr[0], np.hstack(arr[1]), arr[2]])
    return np.hstack([arr[0], np.hstack(arr[1])])

def split_data(arr, numcols, mode_train=True):
    #TODO dirty hack remove !
    if arr.shape[1] % 2:
        mode_train = False
    if mode_train:
        ids, data, y = split_data_train(arr)
        return ids, np.hsplit(data, numcols), y
    ids, data = split_data_test(arr)
    return ids, np.hsplit(data, numcols)

def make_pairs(arr):
    return np.hsplit(np.repeat(arr, 2)[1:-1].reshape(-1,2),2)


