from pathlib import Path
from fishing.data.cache import ArrayCache
from fishing.data.loader import DataReader
from fishing.params import CParams
from fishing.output import out_transform
from fishing.features.lstm import FeaturesLSTM
from fishing.models.lstm import LSTMModel
import csv

LIMIT = 30 #FOR data loader only
CACHE_DIR = Path('/media/ssd_data/tmp/fishing')
RAW_CACHE_DIR = CACHE_DIR / Path('raw_cache.bc')
DATA_DIR = Path('/media/data/topcoder/FishingForFishermen2/input')
SEQLEN = 200
TENSORBOARD_DIR = '/media/ssd_data/tmp/fishing_tb'

COLS = [0,1,2,3,4,5,6]
COLS_STR = "_".join([str(i) for i in COLS])

def load_train_data():
    return DataReader(DATA_DIR, RAW_CACHE_DIR, COLS).get_data_train(LIMIT)

def load_test_data():
    return DataReader(DATA_DIR, RAW_CACHE_DIR, COLS).get_data_test()

def lstm_transformer_train(data, cparams):
    trans = FeaturesLSTM()
    return trans.transform_train(data, cparams)

def lstm_transformer_test(data, cparams):
    trans = FeaturesLSTM()
    return trans.transform_test(data, cparams)


#TODO make it more general, not LSTM only
def load_cache(cparams: CParams, dataloader, transformer):
    cache = ArrayCache(CACHE_DIR)
    try:
        res = cache.load_arr(cparams)
    except FileNotFoundError as e:
        print(e)
        res = dataloader()
        res = transformer(res, cparams)
        cache.save_arr(res, cparams, attrs=cparams._asdict())
        print("Cache builded {}".format(str(cparams)))
    return res

def cparam_test(cparams: CParams):
     c = CParams(**cparams._asdict())
     c.prefix = "test"
     return c

def train(X,y):
    pass



if __name__ == '__main__':
    #Prefix, SeqLen, Overlap, Limit
    cparams = CParams("train7_{}".format(LIMIT), SEQLEN, COLS_STR)
    X = load_cache(cparams, load_train_data, lstm_transformer_train)
    print(X[0].shape)
    print(X[1][0].shape)
    print(len(X[1]))
    print(X[2].shape)
    lstm = LSTMModel(cparams, CACHE_DIR, TENSORBOARD_DIR)
    lstm.fit(X)
    print("Score: ", lstm.calc_score(X))
    exit(0)
    X_test = load_cache(CParams("test7".format(LIMIT), SEQLEN, COLS_STR), load_test_data, lstm_transformer_test)
    ids, y_pred = lstm.predict(X_test)
    res = out_transform(ids, y_pred)
    print(res.shape)
    print(res[:2])
    out_file = str(CACHE_DIR / Path('algis-FishingForFishermenContest.csv'))
    with open(out_file,'w') as f:
        writer = csv.writer(f, quoting=csv.QUOTE_NONE)
        #writer.writerow(ar.dtype.names)
        for row in res:
            writer.writerow([int(float(row[0])), row[1], float(row[2])])
