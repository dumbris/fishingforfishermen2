from keras.utils import to_categorical
import numpy as np

def equalwidth_encoding_1d(arr, n):
    """
    # equal-width discretization
EqualWidth <- function(x,n){
  x <- HampelFilter(x, 5)
  x <- x$y
  v_min <- min(x)
  v_max <- max(x)
  interval <- (v_max - v_min) / n
  print(paste0("min:",v_min))
  print(paste0("max:",v_max))
  print(paste0("interval:",interval))
  
  bin <- floor((x-v_min)/interval) + 1
  member <- (x-v_min)%%interval / interval
  bin[bin==(n+1)] <- n
  
  mm <- matrix(0, length(x), n)
  encoding <- apply(mm, c(1, 2), function(x) 0)
  for(i in c(1:length(x))) {
      encoding[i,bin[i]] <- 1
  }
  encoding <- data.frame(encoding)
  
  stopifnot(sum(encoding)==nrow(encoding))
  
  encoding
}
    """

    bins = np.linspace(np.min(arr, axis=0), np.max(arr, axis=0), n)
    inds = np.digitize(arr, bins) - 1
    return to_categorical(inds, n)


def equalwidth_encoding(arr, n):
    orig_shape = arr.shape
    arr = arr.reshape(-1,1)
    bins = np.linspace(np.min(arr, axis=0), np.max(arr, axis=0), n)
    inds = np.digitize(arr, bins) - 1
    return to_categorical(inds, n).reshape(orig_shape + (n,))