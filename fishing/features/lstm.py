from fishing.features.base import BaseTransformer
from fishing.params import CParams
import fishing.helpers as helpers
from fishing.features.encoding import equalwidth_encoding
import numpy as np

#EARTH_RADIUS = 3958.75
EARTH_RADIUS = 6372.8 #for km

class FeaturesLSTM(BaseTransformer):
    """
    cols = ["TrackNumber",
    "Time(seconds)",
    "Latitude",
    "Longitude",
    "SOG",
    "oceanic depth",
    "Chlorophyll Concentration",
    "Salinity",
    "Water Surface Elevation",
    "Sea Temperature",
    "Thermocline Depth",
    "Eastward Water Velocity",
    "Northward Water Velocity", "target"]
    """

    def __init__(self):
        pass

    def transform_train(self, data, cparams:CParams):
        return self.transform_data(data, cparams.seqlen)

    def transform_test(self, data, cparams:CParams):
        return self.transform_data(data, cparams.seqlen, mode_train=False)

    def make_point_pairs(self, lat, lon):
        lat_1, lat_2 = helpers.make_pairs(lat)
        lon_1, lon_2 = helpers.make_pairs(lon)
        return np.hstack([lat_1, lon_1]), np.hstack([lat_2, lon_2])


    def get_distances(self, locs_1, locs_2):
        #lats1, lats2 = np.meshgrid(locs_1[:,0], locs_2[:,0])
        #lons1, lons2 = np.meshgrid(locs_1[:,1], locs_2[:,1])
        lats1, lats2 = locs_1[:,0], locs_2[:,0]
        lons1, lons2 = locs_1[:,1], locs_2[:,1]

        lat_dif = np.radians(lats1 - lats2)
        long_dif = np.radians(lons1 - lons2)

        sin_d_lat = np.sin(lat_dif / 2.)
        sin_d_long = np.sin(long_dif / 2.)

        step_1 = (sin_d_lat ** 2) + (sin_d_long ** 2) * np.cos(np.radians(lats1[0])) * np.cos(np.radians(lats2[0]))
        step_2 = 2 * np.arctan2(np.sqrt(step_1), np.sqrt(1-step_1))

        dist = step_2 * EARTH_RADIUS

        return dist

    def pad1d(self, arr, seqlen):
        if arr.size > seqlen:
            return arr
        result = np.zeros((seqlen,))
        if arr.size > 0:
            result[:arr.shape[0]] = arr
        return result
        #return result[:,np.newaxis]


    def transform_y(self, arr_y, dim):
        #additional check
        assert len(np.unique(arr_y)) == 1
        return np.full(dim, arr_y[0])

    def reshape_col(self, arr, seqlen):
        l = arr.shape[0]
        rem = l % seqlen
        res = arr[:l - rem].reshape(-1,seqlen)
        if rem > 0:
            padded = self.pad1d(arr[l - rem:].T[0], seqlen)
            res = np.vstack([res, padded])
        return res

    def split_data(self, arr):
        y = arr[:,-1:]
        ids = arr[:,:1]
        data = arr[:,1:-1]
        return ids, data, y

    def split_data_test(self, arr):
        ids = arr[:,:1]
        data = arr[:,1:]
        return ids, data

    def transform_for_1id(self,arr,seqlen):
        res = []
        #calc distance and speed here
        times, lats, lons = arr[:, [0]].astype('float32'), arr[:, [1]].astype('float32'), arr[:, [2]].astype('float32')
        locs1, locs2 = self.make_point_pairs(lats, lons)
        distance = self.get_distances(locs1,locs2).reshape(-1,1)
        arr[:, 1] = np.insert(distance[:,0], 0, 0)
        t1, t2 = helpers.make_pairs(times)
        speed = distance / ((t2 - t1)/3600)
        arr[:, 2] = np.insert(speed[:,0], 0, 0)
        #Transform each column
        for col in range(0, arr.shape[1]):
            res.append(self.reshape_col(arr[:, [col]], seqlen))
        return res


    def transform_data(self, _arr, seqlen, mode_train=True):
        """
        "Time", - 0
        "Latitude", - 1
        "Longitude", - 2
        "SOG", - 3
        oceanic depth - 4,
        Chlorophyll Concentration - 5
        """
        if mode_train:
            ids, arr, y = self.split_data(_arr)
        else:
            ids, arr = self.split_data_test(_arr)
        #encode speed
        #arr[:, [3]] = equalwidth_encoding(arr[:, [3]].astype('float32'),50)
        arr_cols_data = []
        arr_y = []
        arr_ids = []
        for id in np.unique(ids):
            mask = np.where(ids[:, 0] == id)
            data_for_id = self.transform_for_1id(arr[mask], seqlen)
            num_rows_for_id = data_for_id[0].shape[0]
            arr_cols_data.append(data_for_id)
            if mode_train:
                arr_y.append(self.transform_y(y[mask], (num_rows_for_id, 1)))
            arr_ids.append(self.transform_y(ids[mask], (num_rows_for_id, 1)))
        if mode_train:
            return np.vstack(arr_ids), [np.vstack(i) for i in zip(*arr_cols_data)], np.vstack(arr_y)
        else:
            return np.vstack(arr_ids), [np.vstack(i) for i in zip(*arr_cols_data)]
