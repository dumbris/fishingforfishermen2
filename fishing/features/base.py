from abc import ABCMeta, abstractmethod

class BaseTransformer(metaclass=ABCMeta):
    @abstractmethod
    def transform_train(self, data):
        pass
