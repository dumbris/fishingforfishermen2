from collections import namedtuple


class ToStr():
    def __str__(self):
        return '__'.join("{!s}_{!s}".format(key,val) for (key,val) in zip(self._fields, [item for item in self]))
        #'__'.join("{!s}_{!s}".format(key,val) for (key,val) in self._asdict().iteritems())
#zip(obj._fields, item for item in self))

class CParams(namedtuple("Cparams", "prefix seqlen cols"), ToStr):
    pass
