import fishing.const as const
import numpy as np


def out_transform(ids, y_pred):
    items = ids.shape[0]
    _classes = const.ALL_CLASSES
    _classes.sort()
    _ids = np.repeat(ids, len(_classes)).reshape(-1, 1)
    _labels = np.tile(np.array(_classes), (items, 1)).reshape(-1,1)
    probs = y_pred.flatten().reshape(-1, 1)
    #print(_ids.shape, _labels.shape, probs.shape)
    return np.hstack([_ids, _labels, probs])

