from sklearn import preprocessing
from typing import List

class Y2bin:
    def __init__(self, _classes: List[str]):
        self.lb = preprocessing.LabelBinarizer()
        _classes.sort()
        self.lb.fit(_classes)

    def transform(self, arr):
        return self.lb.transform(arr)

    def inverse_transform(self, arr):
        return self.lb.inverse_transform(arr)
