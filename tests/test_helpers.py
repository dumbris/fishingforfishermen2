import pytest
import fishing.helpers as helpers
import numpy as np

@pytest.mark.parametrize("a,b", [
    [
        np.array([[0],[1],[2],[3]]),
        [
            np.array([[0],
                      [1],
                      [2]]),
            np.array([[1],
                      [2],
                      [3]])
        ]
    ]
]
                         )
def test_make_pairs(a,b):
    res = helpers.make_pairs(a)
    np.testing.assert_array_equal(res, b)

