import pytest
from fishing.y2bin import Y2bin
import numpy as np
import fishing.const as const
from sklearn import metrics



class TestScoring:
    def setup(self):
        print("basic setup into class")

    def teardown(self):
        print("basic teardown into class")

    def setup_class(cls):
        print("class setup")

    def teardown_class(cls):
        print("class teardown")

    def setup_method(self, method):
        print("method setup")

    def teardown_method(self, method):
        print("method teardown")

    @pytest.mark.parametrize("a,b", [
            [
            np.array(['trawler', 'longliner', 'seiner', 'other', 'support']),
            np.array(['trawler', 'longliner', 'seiner', 'other', 'support']),
            ],
        ]
        )
    def test_roc_curve(self, a, b):
        print(fpr)
        y = np.array([1, 1, 2, 2])
        scores = np.array([0.1, 0.4, 0.35, 0.8])
        fpr, tpr, thresholds = metrics.roc_curve(y, scores, pos_label=2)
        print(fpr)
        print(tpr)



from sklearn import metrics
import numpy as np

items = 5

scores = np.array([[ 0.9,  0.3,  0.3,  0.3,  0.3],
               [ 0.3,  0.8,  0.3,  0.3,  0.3],
               [ 0.3,  0.3,  0.7,  0.3,  0.3],
               [ 0.3,  0.3,  0.3,  0.8,  0.3],
               [ 0.3,  0.3,  0.3,  0.3,  0.3]])#.flatten().reshape(-1, 1)

y_test = np.array([[1, 0, 0, 0, 0],
              [0, 1, 0, 0, 0],
              [0, 0, 1, 0, 0],
              [0, 0, 0, 1, 0],
              [0, 0, 0, 0, 1]])#.flatten().reshape(-1, 1)

t = np.tile(np.array(['longliner', 'other', 'seiner', 'support', 'trawler']), (items, 1))

weights = np.array([0.2, 0, 0.3, 0.1, 0.4])
# Compute ROC curve and ROC area for each class
fpr = dict()
tpr = dict()
roc_auc = np.zeros((scores.shape[1]))
for i in range(0,scores.shape[1]):
    fpr[i], tpr[i], _ = metrics.roc_curve(y_test[:,i], scores[:,i])
    roc_auc[i] = metrics.auc(fpr[i], tpr[i])
    #res = weights[i] * roc_auc[i]

np.max(1000000 * (2*np.average(roc_auc, weights=weights) - 1), 0)
