import pytest
from fishing.features.lstm import FeaturesLSTM
import numpy as np


class TestFeaturesLSTM:
    def setup(self):
        print("basic setup into class")

    def teardown(self):
        print("basic teardown into class")

    def setup_class(cls):
        print("class setup")

    def teardown_class(cls):
        print("class teardown")

    def setup_method(self, method):
        print("method setup")
        self.transformer = FeaturesLSTM()

    def teardown_method(self, method):
        print("method teardown")

    @pytest.mark.parametrize("a,seqlen,b", [
        [np.array([1,2,3,4]), 6, np.array([1,2,3,4,0,0])],
        [np.array([]), 3, np.array([0,0,0])],
        [np.array([1,2,3,4]), 2, np.array([1,2,3,4])]
        ]
        )
    def test_pad1d(self, a, seqlen, b):
        res = self.transformer.pad1d(a, seqlen)
        np.testing.assert_array_equal(res, b)

    @pytest.mark.parametrize("a,ids,data,y", [
        [
            np.array([[1, 2, 3, 10],
                [4, 5, 6, 11]]),
            np.array([[1],[4]]),
            np.array([[2, 3],[5, 6]]),
            np.array([[10],[11]]),
        ],
        ]
        )
    def test_splitdata(self, a, ids, data, y):
        _ids,_data,_y = self.transformer.split_data(a)
        np.testing.assert_array_equal(_ids, ids)
        np.testing.assert_array_equal(_data, data)
        np.testing.assert_array_equal(_y, y)

    @pytest.mark.parametrize("a,seqlen,b", [
        [   np.array([[1],
                      [2],
                      [3],
                      [4]]),
            2,
            np.array([[1,2],
                      [3,4]])],
        [   np.array([[1],
                      [2],
                      [3],
                      [4],
                      [5]]),
            3,
            np.array([[1,2,3],
                      [4,5,0]])],
        [   np.array([[1],
                      [2],
                      [3],
                      [4]]),
            3,
            np.array([[1,2,3],
                      [4,0,0]])],
        [   np.array([[1],
                      [2],
                      [3],
                      [4]]),
            10,
            np.array([[1,2,3,4,0,0,0,0,0,0]])],
        [   np.array([[1],
                      [2],
                      [3]]),
            3,
            np.array([[1,2,3]])]
        ]
        )
    def test_reshape_col(self, a, seqlen, b):
        res = self.transformer.reshape_col(a, seqlen)
        np.testing.assert_array_equal(res, b)

    @pytest.mark.parametrize("a,b1,b2,b3", [
        [
            np.array([[1, 2, 3],
                      [4, 5, 6],
                      [7, 8, 9],
                      [10, 11, 12],
                      [13, 14, 15],
                      ]),
            np.array([[1,4,7],[10,13,0]]),
            np.array([[2, 5, 8],[11, 14, 0]]),
            np.array([[3, 6, 9],[12, 15, 0]]),
        ],
        ]
        )
    def test_transform_for1id(self, a, b1, b2, b3):
        res = self.transformer.transform_for_1id(a,3)
        np.testing.assert_array_equal(res[0], b1)
        np.testing.assert_array_equal(res[1], b2)
        np.testing.assert_array_equal(res[2], b3)

    @pytest.mark.parametrize("a,b", [
        [
            np.array([[100, 1, 2, 3, 999],
                      [100, 4, 5, 6, 999],
                      [100, 7, 8, 9, 999],
                      [100, 10, 11, 12, 999],
                      [100, 13, 14, 15, 999],
                      [101, 16, 17, 18, 990],
                      [101, 19, 20, 21, 990],
                      ]),
            (np.array([[100],[100],[101]]),
            [
                np.array([[1,4,7],[10,13,0],[16,19,0]]),
                np.array([[2, 5, 8],[11, 14, 0], [17, 20, 0]]),
                np.array([[3, 6, 9],[12, 15, 0], [18, 21, 0]])
            ],
             np.array([[999],[999],[990]])
            )
        ],
        ]
        )
    def test_transform_data(self, a, b):
        res = self.transformer.transform_data(a,3)
        for i in range(0,3):
            np.testing.assert_array_equal(res[i], b[i])

    @pytest.mark.parametrize("a,shape,b", [
        [
            np.array([[1], [1], [1]]),
            (2,1),
            np.array([[1],[1]]),
        ],
        ]
        )
    def test_transform_y(self, a, shape, b):
        _y = self.transformer.transform_y(a, shape)
        np.testing.assert_array_equal(_y, b)


    def test_hsplit(self):
        a =           [
                        np.array([[1,4,7],[10,13,0],[16,19,0]]),
                        np.array([[2, 5, 8],[11, 14, 0], [17, 20, 0]]),
                        np.array([[3, 6, 9],[12, 15, 0], [18, 21, 0]])
                    ]
        b = np.hstack(a)
        c = np.hsplit(b, len(a))
        np.testing.assert_array_equal(a,c)

    @pytest.mark.parametrize("lat, lon, b", [
        [
            np.array([[0],[1],[2],[3]]),
            np.array([[4],[5],[7],[8]]),
           (np.array([[0, 4],
                      [1, 5],
                      [2, 7]]),
            np.array([[1, 5],
                      [2, 7],
                      [3, 8]]))
        ],
    ]
                             )
    def test_make_point_pairs(self, lat, lon, b):
        res = self.transformer.make_point_pairs(lat, lon)
        np.testing.assert_array_equal(res, b)
