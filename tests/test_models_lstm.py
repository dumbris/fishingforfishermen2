import pytest
from fishing.models.lstm import LSTMModel
from fishing.params import CParams
from pathlib import Path
import numpy as np


class TestFeaturesLSTM:
    def setup(self):
        print("basic setup into class")

    def teardown(self):
        print("basic teardown into class")

    def setup_class(cls):
        print("class setup")

    def teardown_class(cls):
        print("class teardown")

    def setup_method(self, method):
        print("method setup")
        self.model = LSTMModel(CParams('test',0,0,0), Path('/tmp'), Path('/tmp'))

    def teardown_method(self, method):
        print("method teardown")

    @pytest.mark.parametrize("ids,probs,out", [

        [np.array([[1],
                   [2],
                   [3],
                   [4],
                   [5]]),
         np.array([[ 0.9,  0.3,  0.3,  0.3,  0.3],
               [ 0.3,  0.8,  0.3,  0.3,  0.3],
               [ 0.3,  0.3,  0.7,  0.3,  0.3],
               [ 0.3,  0.3,  0.3,  0.8,  0.3],
               [ 0.3,  0.3,  0.3,  0.3,  0.3]]),
         np.array([])
        ]
        ]
        )
    def test_pad1d(self, ids, probs, out):
        res = self.model.out_transform(ids, probs)
        np.testing.assert_array_equal(res, out)
