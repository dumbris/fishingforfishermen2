import pytest
import os
import fishing
from fishing.data.loader import Data
import argparse

class TestCache:

def test_cache():
    try:
        X = load_arr((prefix, size, limit))
    except FileNotFoundError as e:
        print(e)
        X = build_cache(img_path, (prefix, size, limit), X_train)
    self.X = X

def test_main(monkeypatch):
    mock = []
    def mock_run(config_name, profile_name, config_dir):
        mock.append('mock_run')
        return

    def mock_parse_args(self, args=None, values=None):
        res = self.parse_known_args()
        return res[0]

    monkeypatch.setattr('sploader.run', mock_run)
    monkeypatch.setattr(argparse.ArgumentParser, 'parse_args', mock_parse_args)

    try:
        main()
        assert 'mock_run' in mock
    except Exception as ex:
        assert False

