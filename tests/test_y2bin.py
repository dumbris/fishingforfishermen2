import pytest
from fishing.y2bin import Y2bin
import numpy as np
import fishing.const as const



class TestY2bin:
    def setup(self):
        print("basic setup into class")

    def teardown(self):
        print("basic teardown into class")

    def setup_class(cls):
        print("class setup")

    def teardown_class(cls):
        print("class teardown")

    def setup_method(self, method):
        print("method setup")
        self.y2bin = Y2bin(const.ALL_CLASSES)

    def teardown_method(self, method):
        print("method teardown")

    @pytest.mark.parametrize("a,b", [
            [
            np.array(['trawler', 'longliner', 'seiner', 'other', 'support']),
            np.array(['trawler', 'longliner', 'seiner', 'other', 'support']),
            ],
        ]
        )
    def test_pad1d(self, a, b):
        a.sort()
        b.sort()
        res = self.y2bin.transform(a)
        np.testing.assert_array_equal(res, b)
